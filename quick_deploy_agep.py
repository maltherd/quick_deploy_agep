import argparse
import yaml
import requests
import docker
from colors import red, green, yellow

# Parse args
parser = argparse.ArgumentParser(description="A quick script to bypass the agepoly gitlab CI which I hate")
parser.add_argument("--tag", type=str, required=True, help="Docker tag to use for the newly built image")
parser.add_argument("--docker-reg-uri", type=str, required=True, help="Public URI of the registry to push the newly built image (some gitlab registry)")
parser.add_argument("--docker-reg-user", type=str, required=True, help="Gitlab username to push the newly built image (some gitlab registry)")
parser.add_argument("--docker-reg-password", type=str, required=True, help="Personal access token to push the newly built image (some gitlab registry)")
parser.add_argument("--yaml", type=str, required=True, help="Name of the yaml production manifest in the gitlab - with extension!")
parser.add_argument("--k8s", action="store_true", help="Format for the `k8s` cluster and remove the `mucci` cluster's flags")
parser.add_argument("--only-yaml", action="store_true", help="Only format the yaml. Do not docker build and push.")

args = parser.parse_args()

print(yellow("[warn]"), "Don't forget to git pull.")

# Get yaml manifest, and change the required values
resp = requests.get(f"https://gitlab.com/agepoly/it/infra/k8s_production_manifests/-/raw/main/production/{args.yaml}")
raw_yaml = resp.content
parsed_yaml = list(yaml.load_all(raw_yaml, Loader=yaml.FullLoader))  # There are multiple documents
print(green("[ ok ]"), "Downloaded yaml production manifest successfully.")
print(yellow("[warn]"), "The manifest's structure may have changed significantly, but this script can't notice. Make sure it's fine to just use it like that.")

try:
    expected_container_name = args.yaml.split(".")[0]  # This is an assumption
    expected_container_found = False
    containers = parsed_yaml[0]["spec"]["template"]["spec"]["containers"]
    for ct in containers:
        if ct["name"] == expected_container_name:
            ct["image"] = f"{args.docker_reg_uri}:{args.tag}"
            expected_container_found = True
            print(green("[ ok ]"), f"Found `{expected_container_name}` container.")
            break

    if not expected_container_found:
        print(red("[fail]"), f"Expected container {expected_container_name} not found !")
        exit()

    if args.k8s:
        del parsed_yaml[0]["spec"]["template"]["spec"]["nodeSelector"]
        busybox_maybe = parsed_yaml[0]["spec"]["template"]["spec"]["initContainers"][0]
        if busybox_maybe["image"].endswith("busybox"):
            busybox_maybe["image"] = "busybox"
        else:
            print(red("[fail]"), f"Expected a busybox as first initContainer. Found `{busybox_maybe['image']}`!")
            exit()

    with open(args.yaml, "w") as f:
        for document in parsed_yaml:
            final_yaml = yaml.dump(document)
            f.write("---\n")
            f.write(final_yaml)
            break  # Actually it turns out we only want the Deployment part of the file. The rest is immutable usually.

        print(green("[ ok ]"), f"Written modified yaml in `{args.yaml}` file. Please `kubectl apply -f` it when the image is pushed.")

except (KeyError, IndexError) as exc:
    print(red("[fail]"), "The yaml structure has changed since writing the script.")
    raise exc

if args.only_yaml:
    exit()

# Docker login
docker_client = docker.from_env()
docker_client.login(username=args.docker_reg_user, password=args.docker_reg_password, registry=args.docker_reg_uri.split("/")[0])  # This is an assumption
print(green("[ ok ]"), "Login to registry.")

# Docker build
print("[....]", "Building docker image...")
docker_image, logs = docker_client.images.build(path=".", tag=args.tag, rm=True, pull=True)
for line in logs:
    text = line.get("stream", line.get("aux", ""))
    print("      ", str(text).rstrip())
print(green("[ ok ]"), "Built docker image.")

# Retag because docker needs its autism together
docker_image.tag(f"{args.docker_reg_uri}:{args.tag}")
print(green("[ ok ]"), "Tagged docker image again for push.")

# Docker push
print("[....]", "Pushing docker image...")
logs = docker_client.api.push(args.docker_reg_uri, tag=args.tag, stream=True, decode=True)
finished = False
for line in logs:
    if "progressDetail" in line and "current" in line["progressDetail"] and "total" in line["progressDetail"]:
        percentage = str(int(int(line["progressDetail"]["current"]) / int(line["progressDetail"]["total"]) * 100))
        status = line["status"]
    elif "status" in line and line["status"] == "Pushed":
        percentage = "100"
        status = line["status"]
    elif "status" not in line:
        percentage = "100"
        status = "Finished"
        finished = True
    else:
        percentage = "0"
        status = line["status"]
    print(f"[{percentage.rjust(3)}%]", status)

if finished:
    print(green("[ ok ]"), "Pushed image.")
else:
    print(red("[fail]"), "Image not pushed.")
